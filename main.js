const rest = require("./restTests");
const grpc = require("./grpcTests");
const { performance } = require("perf_hooks");

const REST_ENDPOINT = "http://18.220.6.41:3000";
const GRPC_ENDPOINT = "18.225.234.75:3001";

const runTest = async (func) => {
  const start = performance.now();
  await func();
  const end = performance.now();
  return end-start;
}

const avgRuntime = async (func, times) => {
  const promises = [];
  for (let i = 0; i < times; i++) {
    promises.push(runTest(func));
  }

  const results = await Promise.all(promises);
  return results.reduce((acc, curr) => acc + curr) / times;
};

async function getExamples() {
  let resp = await fetch(REST_ENDPOINT + "/randomproduct");
  let body = await resp.json();
  const prodId = body.id;

  resp = await fetch(REST_ENDPOINT + "/users");
  body = await resp.json();
  const userId = body[0].id;

  return { product: prodId, user: userId };
}

async function runTests() {
  const {user, product} = await getExamples();

  const ITERATIONS = 100;

//  console.log("-----GRPC TESTS-----");
//  for (const test of grpc.getTests(GRPC_ENDPOINT, user, product)) {
//    console.log(test.name);
//    console.log(await avgRuntime(test.run, ITERATIONS));
//  }

  console.log("-----REST TESTS-----");
  for (const test of rest.getTests(REST_ENDPOINT, user, product)) {
    console.log(test.name);
    console.log(await avgRuntime(test.run, ITERATIONS));
  }
}

runTests();
