const grpc = require("@grpc/grpc-js");
const protoLoader = require("@grpc/proto-loader");

const PROTO_PATH = "./app.proto";

const packageDefinition = protoLoader.loadSync(PROTO_PATH, {
  keepCase: true,
  longs: String,
  enums: String,
  defaults: true,
  oneofs: true,
});

const app = grpc.loadPackageDefinition(packageDefinition);

const getTests = (endpoint, user, product) => {
  const client = new app.App(endpoint, grpc.credentials.createInsecure());

  return [
    {
      name: "randomProduct",
      run: async () => {
        const response = await new Promise((resolve, reject) => {
          client.getRandomProduct({}, function (_err, response) {
            if (_err) reject(_err);
            resolve(response);
          });
        });

        return [0, response];
      },
    },
    {
      name: "product",
      run: async () => {
        const response = await new Promise((resolve, reject) => {
          client.getProduct(
            { product_id: product },
            function (_err, response) {
              if (_err) reject(_err);
              resolve(response);
            },
          );
        });

        return [0, response];
      },
    },
    {
      name: "allProducts",
      run: async () => {
        const response = await new Promise((resolve, reject) => {
          client.getAllProducts({}, function (_err, response) {
            if (_err) reject(_err);
            resolve(response);
          });
        });

        return [0, response];
      },
    },
    {
      name: "categories",
      run: async () => {
        const response = await new Promise((resolve, reject) => {
          client.getAllCategories({}, function (_err, response) {
            if (_err) reject(_err);
            resolve(response);
          });
        });

        return [0, response];
      },
    },
    {
      name: "allOrders",
      run: async () => {
        const response = await new Promise((resolve, reject) => {
          client.getAllOrders({}, function (_err, response) {
            if (_err) reject(_err);
            resolve(response);
          });
        });

        return [0, response];
      },
    },
    {
      name: "ordersByUser",
      run: async () => {
        const response = await new Promise((resolve, reject) => {
          client.getAllUserOrders(
            { id: user },
            function (_err, response) {
              if (_err) reject(_err);
              resolve(response);
            },
          );
        });

        return [0, response];
      },
    },
    {
      name: "user",
      run: async () => {
        const response = await new Promise((resolve, reject) => {
          client.getUser(
            { id: user },
            function (_err, response) {
              if (_err) reject(_err);
              resolve(response);
            },
          );
        });

        return [0, response];
      },
    },
    {
      name: "allUsers",
      run: async () => {
        const response = await new Promise((resolve, reject) => {
          client.getAllUsers({}, function (_err, response) {
            if (_err) reject(_err);
            resolve(response);
          });
        });

        return [0, response];
      },
    },
    {
      name: "postOrder",
      run: async () => {
        {
          const response = await new Promise((resolve, reject) => {
            client.postOrder(
              {
                user_id: user,
                products: [
                  {
                    product_id: product,
                    quantity: 2,
                  },
                ],
                total_amount: 600.0,
              },
              function (_err, response) {
                if (_err) reject(_err);
                resolve(response);
              },
            );
          });

          return [0, response];
        }
      },
    },
    {
      name: "patchUser",
      run: async () => {
        const response = await new Promise((resolve, reject) => {
          client.patchAccountDetails(
            {
              id: user,
              email: "update@test.com",
              name: "update test",
            },
            function (_err, response) {
              if (_err) reject(_err);
              resolve(response);
            },
          );
        });

        return [0, response];
      },
    },
  ];
};

module.exports = { getTests };
