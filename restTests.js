const getTests = (endpoint, user, product) => [
  //  {
  //    run: async () => {
  //      const resp = await fetch(endpoint + "/");
  //      const body = await resp.json();
  //      return [resp.status, body];
  //    },
  //  },
  {
    name: "randomProduct",
    run: async () => {
      const resp = await fetch(endpoint + "/randomproduct");
      const body = await resp.json();
      return [resp.status, body];
    },
  },
  {
    name: "product",
    run: async () => {
      const resp = await fetch(
        `${endpoint}/product/${product}`,
      );
      const body = await resp.json();
      return [resp.status, body];
    },
  },
  {
    name: "allProducts",
    run: async () => {
      const resp = await fetch(endpoint + "/products");
      const body = await resp.json();
      return [resp.status, body];
    },
  },
  {
    name: "categories",
    run: async () => {
      const resp = await fetch(endpoint + "/categories");
      const body = await resp.json();
      return [resp.status, body];
    },
  },
  {
    name: "allOrders",
    run: async () => {
      const resp = await fetch(endpoint + "/allorders");
      const body = await resp.json();
      return [resp.status, body];
    },
  },
  {
    name: "ordersByUser",
    run: async () => {
      const resp = await fetch(`${endpoint}/orders?userId=${user}`);
      const body = await resp.json();
      return [resp.status, body];
    },
  },
  {
    name: "user",
    run: async () => {
      const resp = await fetch(
        `${endpoint}/user/${user}`,
      );
      const body = await resp.json();
      return [resp.status, body];
    },
  },
  {
    name: "allUsers",
    run: async () => {
      const resp = await fetch(endpoint + "/users");
      const body = await resp.json();
      return [resp.status, body];
    },
  },
  {
    name: "postOrder",
    run: async () => {
      const resp = await fetch(endpoint + "/orders", {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          user_id: user,
          products: [
            {
              product_id: product,
              quantity: 2,
            },
          ],
          total_amount: 600.0,
        }),
      });

      const body = null;

      return [resp.status, body];
    },
  },
  {
    name: "patchUser",
    run: async () => {
      const resp = await fetch(
        endpoint + `/user/${user}`,
        {
          method: "PATCH",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            id: user,
            email: "update@test.com",
            name: "update test",
          }),
        },
      );

      const body = null;

      return [resp.status, body];
    },
  },
];

module.exports = { getTests };
